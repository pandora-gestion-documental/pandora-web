---
title: 2-Archivos
---

Ofrecemos soluciones encaminadas a mejorar las condiciones de los archivos, tanto **físicas** como de **acceso y recuperación**, para organismos públicos y privados.

La **concienciación** en cuanto a la necesidad de disponer de un archivo en óptimas condiciones ha supuesto un **ahorro** e incluso un **beneficio** para muchas empresas. En el caso de las administraciones públicas, además les ayuda a cumplir con la **legislación vigiente** en materia de Patrimonio, acceso a la información, transparencia, etc.

Coordinamos proyectos para cualquier tipo de institución y en todos los **ciclos vitales** de sus documentos, acometiendo **trabajos archivísticos** de identificación de fondos, clasificación, descripción, elaboración de calendarios de conservación, ordenación e instalación.
